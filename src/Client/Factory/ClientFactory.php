<?php

namespace UltimaClient\Client\Factory;

use Interop\Container\ContainerInterface;
use UltimaClient\Client\Client;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Http\Client as HttpClient;

class ClientFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config')['ultima-api-client'];

        if (empty($config['endpoint'])) {
            throw new \Exception('Api endpoint is not provided');
        }

        if (empty($config['login'])) {
            throw new \Exception('Login is not provided');
        }

        if (empty($config['password'])) {
            throw new \Exception('Password is not provided');
        }

        $httpClient = new HttpClient();

        if (!empty($config['options'])) {
            $httpClient->setOptions($config['options']);
        }

        $client = new Client($httpClient);
        $client->setEndpoint($config['endpoint']);
        $client->setLogin($config['login']);
        $client->setPassword($config['password']);

        return $client;
    }
}