<?php

namespace UltimaClient\Client;

use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Session\Container;

class Client
{
    const AUTH_COOKIE_NAME = 'ss-id';
    const SSID_STORAGE_KEY = 'ssid';

    /** @var HttpClient  */
    private $httpClient;

    /** @var string */
    private $endpoint;

    /** @var string */
    private $login;

    /** @var string */
    private $password;

    /** @var bool */
    private $useSsId = true;

    /** @var Container */
    private $session;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $resource
     * @param array $params
     * @return mixed|Response
     */
    public function get($resource, $params = [])
    {
        return $this->request($resource, $params);
    }

    /**
     * @param $resource
     * @param array $data
     * @return mixed|Response
     */
    public function post($resource, $data = [])
    {
        return $this->request($resource, $data, Request::METHOD_POST);
    }

    /**
     * @param $resource
     * @param array $params
     * @param string $method
     * @return mixed|Response
     * @throws \Exception
     */
    public function request($resource, $params = [], $method = Request::METHOD_GET)
    {
        $this->httpClient->setUri($this->endpoint . $resource);
        $this->httpClient->setMethod($method);

        if ($this->isUseSsId() && $this->hasSsId()) {
            $this->httpClient->addCookie(self::AUTH_COOKIE_NAME, $this->getSsId());
        } else {
            $this->httpClient->setAuth($this->login, $this->password);
        }

        if (count($params) > 0) {
            $this->httpClient->getRequest()->setContent(json_encode($params));
            $this->httpClient->getRequest()->getHeaders()->addHeaders(array(
                'Content-Type: application/json',
            ));
        }

        $response = $this->httpClient->send();

        if ($this->isUseSsId() && $response->getStatusCode() == Response::STATUS_CODE_401) {
            $this->setUseSsId(false);
            return $this->request($resource, $params, $method);
        }

        if (!$response->isSuccess()) {
            throw new \Exception(
                'UltimaApi error: '
                . $response->getStatusCode()
                . ' ' . $response->getReasonPhrase()
                . ': ' . $this->endpoint . $resource . ' Params: ' . print_r($params, true),
                $response->getStatusCode()
            );
        }

        if (!$this->isUseSsId()) {
            $ssId = $this->getSsIdFromCookie($response);
            $this->setSsId($ssId);
        }

        $response = json_decode($response->getBody());

        if (is_null($response)) {
            throw new \Exception('UltimaApi error: cannot parse json. ' . json_last_error());
        }

        return $response;
    }

    /**
     * @param $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param Response $response
     * @return null|string
     */
    private function getSsIdFromCookie(Response $response)
    {
        foreach ($response->getCookie() as $cookie) {
            if ($cookie->getName() == self::AUTH_COOKIE_NAME) {
                return $cookie->getValue();
            }
        }

        return null;
    }

    /**
     * @param $ssId
     */
    private function setSsId($ssId)
    {
        if (is_null($this->session)) {
            $this->session = new Container(self::SSID_STORAGE_KEY);
        }

        $this->session->ssId = $ssId;
    }

    /**
     * @return bool
     */
    private function hasSsId()
    {
        if (is_null($this->session)) {
            $this->session = new Container(self::SSID_STORAGE_KEY);
        }

        return !empty($this->session->ssId);
    }

    /**
     * @return mixed
     */
    private function getSsId()
    {
        if (is_null($this->session)) {
            $this->session = new Container(self::SSID_STORAGE_KEY);
        }

        return $this->session->ssId;
    }

    /**
     * @return bool
     */
    private function isUseSsId()
    {
        return $this->useSsId;
    }

    /**
     * @param bool $useSsId
     */
    private function setUseSsId($useSsId)
    {
        $this->useSsId = $useSsId;
    }
}