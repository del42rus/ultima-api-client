<?php

namespace UltimaClient;

use UltimaClient\Client\Client;
use UltimaClient\Client\Factory\ClientFactory;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Client::class => ClientFactory::class
            ]
        ];
    }
}