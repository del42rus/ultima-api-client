<?php

namespace Ultima\ApiClient;

use Zend\Http\Client\Adapter\Curl;

return [
    'ultima-api-client' => [
        'endpoint' => '',
        'login' => '',
        'password' => '',
        'options' => [
            'adapter' => Curl::class
        ]
    ]
];