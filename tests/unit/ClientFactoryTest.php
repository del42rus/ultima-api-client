<?php

use Mockery\MockInterface;
use UltimaClient\Client\Factory\ClientFactory;
use Zend\ServiceManager\ServiceManager;
use UltimaClient\Client\Client as ApiClient;

class ClientFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var ClientFactory */
    protected $factory;

    /** @var ServiceManager|MockInterface */
    protected $serviceManager;

    protected function _before()
    {
        $this->serviceManager = \Mockery::mock(ServiceManager::class);
        $this->factory = new ClientFactory();
    }

    protected function _after()
    {
        Mockery::close();
    }

    protected function getApiClientConfig()
    {
        return [
            'ultima-api-client' => [
                'endpoint' => 'endpoint',
                'login' => 'login',
                'password' => 'password'
            ]
        ];
    }

    /**
     * @dataProvider getEmptyParameterExceptions
     */
    public function testWrongConfig($parameter, $exception, $exceptionMessage)
    {
        $this->expectException($exception);
        $this->expectExceptionMessage($exceptionMessage);

        $config = $this->getApiClientConfig();
        unset($config['ultima-api-client'][$parameter]);

        $this->serviceManager->shouldReceive('get')->with('Config')->andReturn(
            $config
        );

        ($this->factory)($this->serviceManager, ApiClient::class);
    }

    public function getEmptyParameterExceptions()
    {
        return [
            'endpoint' => ['endpoint', Exception::class, 'Api endpoint is not provided'],
            'login' =>  ['login', Exception::class, 'Login is not provided'],
            'password' => ['password', Exception::class, 'Password is not provided'],
        ];
    }

    public function testValidApiClientInstance()
    {
        $config = $this->getApiClientConfig();

        $this->serviceManager->shouldReceive('get')->with('Config')->andReturn(
            $config
        );

        $client = ($this->factory)($this->serviceManager, ApiClient::class);

        $this->assertInstanceOf(ApiClient::class, $client);
    }
}