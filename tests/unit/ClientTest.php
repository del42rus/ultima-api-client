<?php

use Mockery\MockInterface;
use Zend\Http\Client\Adapter\Curl;
use Zend\ServiceManager\ServiceManager;
use UltimaClient\Client\Client as ApiClient;
use UltimaClient\Client\Factory\ClientFactory;

class ClientTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var ClientFactory */
    protected $factory;

    /** @var ServiceManager|MockInterface */
    protected $serviceManager;

    protected function _before()
    {
        $this->serviceManager = \Mockery::mock(ServiceManager::class);
        $this->factory = new ClientFactory();
    }

    protected function _after()
    {
        Mockery::close();
    }

    public function getApiClient()
    {
        $config = [
            'ultima-api-client' => [
                'endpoint' => 'http://localhost/',
                'login' => 'login',
                'password' => 'password',

                'options' => [
                    'adapter' => Curl::class
                ]
            ]
        ];

        $this->serviceManager->shouldReceive('get')->with('Config')->andReturn(
            $config
        );

        $client = ($this->factory)($this->serviceManager, ApiClient::class);

        return $client;
    }

    public function testWrongRequest()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('UltimaApi error:');

        $client = $this->getApiClient();

        $client->get('something');
    }
}